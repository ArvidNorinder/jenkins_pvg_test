package result;

import java.time.LocalTime;

import result.marathon.MarathonDriver;

/**
 * Container for raw data read from files, pertaining to a specific driver.
 * Sub classes for different driver types, e.g. MarathonDriver.
 */
public abstract class Driver implements Comparable {

    protected final String startNumber;
    private String name;

    public Driver(String startNumber) {
        try {
            Integer.parseInt(startNumber);
        }
        catch (Exception ex) {
            System.out.println("[WARNING] Driver instantiated with non-numeric start number!");
        }
        this.startNumber = startNumber;
    }

    public String getStartNumber() {
        return startNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        try {
            return getStartNumber() == ((Driver)o).getStartNumber();
        }
        catch (Exception ex) {
            return false;
        }
    }

    public abstract CharSequence toFileFormat(boolean sorted);

    public abstract LocalTime getTotalTime();
}
