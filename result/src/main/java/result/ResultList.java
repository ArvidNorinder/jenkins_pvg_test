package result;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import result.marathon.MarathonDriver;
import result.marathon.error.MarathonDecorator;

import java.io.FileWriter;
import java.io.IOException;;

public class ResultList implements Collection<Driver> {
    private List<Driver> drivers;
    private boolean sorted = false;

    public ResultList() {
        drivers = new ArrayList<>();
    }

    public void ReadFile(Path path) throws IOException {
        List<String> lines = Files.readAllLines(path);
        for (int i = 0; i < lines.size(); i++) {
            String[] arr = lines.get(i).split(";");
            String startNumber = arr[0];
            LocalTime time = LocalTime.parse(arr[1], DateTimeFormatter.ofPattern("HH.mm.ss"));
            boolean isStart = arr[2].equals("starttid");
            sortByStartNumber();
            MarathonDriver driver = new MarathonDriver(startNumber);
            if (contains(driver)) {
                driver = (MarathonDriver) get(Integer.parseInt(startNumber) - 1);
            }
            else add(driver);
            
            if (isStart) driver.setStart(time);
            else driver.setEnd(time);
        }
    }

    public void WriteFile(String path) throws IOException {
        FileWriter fileWriter = new FileWriter(path);
        fileWriter.write("");
        if (sorted) fileWriter.append("PlaceringsNr; StartNr; Totaltid\n");
        else fileWriter.append("StartNr; Totaltid; Starttid; Måltid\n");
        int idx = 1;
        for (Driver driver : drivers) {
            if (sorted && driver.getTotalTime() != null) fileWriter.append(Integer.toString(idx) + " ");
            else if (sorted) fileWriter.append("- ");
            fileWriter.append(driver.toFileFormat(sorted) + "\n");
            idx++;
        }
        fileWriter.close();
    }

    public void sort() {
        drivers = drivers.stream().sorted().collect(Collectors.toList());
        sorted = true;
    }

    public void sortByStartNumber() {
        drivers.sort((Driver d1, Driver d2) -> 
        Integer.parseInt(d1.getStartNumber()) - Integer.parseInt(d2.getStartNumber()));
        sorted = false;
    }

    public Driver get(int index) {
        return drivers.get(index);
    }

    @Override
    public int size() {
        return drivers.size();
    }

    @Override
    public boolean isEmpty() {
        return drivers.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return drivers.contains(o);
    }

    @Override
    public Iterator<Driver> iterator() {
        return drivers.iterator();
    }

    @Override
    public Driver[] toArray() {
        int size = size();
        Driver[] arr = new Driver[size];
        for (int i = 0; i < size; i++) {
            arr[i] = drivers.get(i);
        }
        return arr;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        // TODO
        return null;
    }

    @Override
    public boolean add(Driver driver) {
        if (drivers.contains(driver)) {
            System.out.println("[WARNING] Trying to dd driver to the list that is already in list. Returning.");
            return false;
            //? Should we throw and exception here?
        }
        return drivers.add((driver));
    }

    @Override
    public boolean remove(Object driver) {
        return drivers.remove(driver);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return drivers.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Driver> c) {
        return drivers.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return drivers.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return drivers.retainAll(c);
    }

    @Override
    public void clear() {
        drivers.clear();
    }
}
