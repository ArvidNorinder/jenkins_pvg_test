package result.marathon;

import java.util.List;

import result.Result;

/**
 * Marathon races always have one start time,  one end time, and a total time.
 */
public interface MarathonResult extends Result {

    List<String> getStart();
    List<String> getEnd();
    String getTotal();
}
