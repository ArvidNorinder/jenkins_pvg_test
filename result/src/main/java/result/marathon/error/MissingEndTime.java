package result.marathon.error;

import java.util.List;

import result.marathon.MarathonResult;

public class MissingEndTime extends MarathonDecorator {

    public MissingEndTime(MarathonResult result) {
        super(result);
    }

    @Override
    public List<String> getEnd() {
        return List.of("Slut?");
    }

    @Override
    public String getTotal() {
        return MISSING_TIME;
    }
}
