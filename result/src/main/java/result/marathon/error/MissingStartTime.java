package result.marathon.error;

import java.util.List;

import result.marathon.MarathonResult;

public class MissingStartTime extends MarathonDecorator {

    public MissingStartTime(MarathonResult result) {
        super(result);
    }

    @Override
    public List<String> getStart() {
        return List.of("Start?");
    }

    @Override
    public String getTotal() {
        return MISSING_TIME;
    }
}
