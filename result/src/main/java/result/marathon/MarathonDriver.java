package result.marathon;

import result.Driver;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Marathon implementation of the Driver class.
 */
public class MarathonDriver extends Driver {

    private List<LocalTime> start;
    private List<LocalTime> end;

    public MarathonDriver(String startNumber) {
        super(startNumber);
        start = new ArrayList<>();
        end = new ArrayList<>();
    }

    public List<LocalTime> getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start.add(start);
    }

    public List<LocalTime> getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end.add(end);
    }

    public CharSequence toRegularFileFormat() {
        String str = "";
        String start = this.start.size() == 0 ? "Start?" : this.start.get(0).format(DateTimeFormatter.ofPattern("HH.mm.ss"));
        String end = this.end.size() == 0 ? "Slut?" : this.end.get(0).format(DateTimeFormatter.ofPattern("HH.mm.ss"));

        String total = getTotalTime() == null ? "--.--.--" : getTotalTime().format(DateTimeFormatter.ofPattern("HH.mm.ss"));
        
        str += getStartNumber() + "; ";

        str += total + "; ";
        str += start + "; ";
        str += end;

        if (this.start.size() > 1) {
            str += "Flera starttider? ";
            for (int i = 1; i < this.start.size(); i++) str += this.start.get(i).format(DateTimeFormatter.ofPattern("HH.mm.ss")) + " ";
        }
        if (this.end.size() > 1) {
            str += "Flera måltider? ";
            for (int i = 1; i < this.end.size(); i++) str += this.end.get(i).format(DateTimeFormatter.ofPattern("HH.mm.ss")) + " ";
        }
        if (getTotalTime() != null && getTotalTime().getSecond() < 60*15) {
            str += "Omöjlig totaltid?";
        }
        return str;
    }

    public CharSequence toSortedFileFormat() {
        String str = "";
        String total = getTotalTime() == null ? "--.--.--" : getTotalTime().format(DateTimeFormatter.ofPattern("HH.mm.ss"));
        str += getStartNumber() + "; ";
        str += total;
        return str;
    }

    @Override
    public CharSequence toFileFormat(boolean sorted) {
        return sorted ? toSortedFileFormat() : toRegularFileFormat();
    }

    @Override
    public LocalTime getTotalTime() {
        if (start.size() == 0 || end.size() == 0) return null;
        return LocalTime.MIN.plusSeconds(start.get(0).until(end.get(0), ChronoUnit.SECONDS));
    }

    @Override
    public int compareTo(Object o) {
        LocalTime myTime = getTotalTime();
        if (myTime == null) return Integer.MAX_VALUE;
        LocalTime otherTime = ((MarathonDriver)o).getTotalTime();
        if (otherTime == null) return Integer.MIN_VALUE;
        return myTime.compareTo(otherTime);
    }
}
