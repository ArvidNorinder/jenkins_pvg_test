package result.marathon;

import result.ResultRow;
import util.TimeUtils;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


// StartNe, Name, Start, End, Duration, erros*


public class MarathonResultRow extends ResultRow implements MarathonResult {
    private List<LocalTime> start;
    private List<LocalTime> end;

    private List<String> formatedTime;

    public MarathonResultRow(String number, String name, List<LocalTime> start, List<LocalTime> end) {
        super(number, name);
        this.start = start;
        this.end = end;
    }

    public List<String> getStart() {
        return start
        .stream()
        .map(t -> TimeUtils.formatTime(t))
        .collect(Collectors.toList());
    }

    public List<String> getEnd() {
        return end
        .stream()
        .map(t -> TimeUtils.formatTime(t))
        .collect(Collectors.toList());
    }

    @Override
    public String getTotal() {
        return TimeUtils.formatTime(Duration.between(start.get(0), end.get(0)));
    }

}
