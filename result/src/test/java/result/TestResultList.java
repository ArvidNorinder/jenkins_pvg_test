package result;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import javax.annotation.processing.Filer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import result.marathon.MarathonDriver;

public class TestResultList {
    private ResultList resultList;
    private MarathonDriver driver1;
    private MarathonDriver driver1_b;
    private MarathonDriver driver2;
    private MarathonDriver driver3;
    private MarathonDriver driver4;

    @BeforeEach
    public void setup() {
        resultList = new ResultList();
        driver1 = new MarathonDriver("1");
        driver1_b = new MarathonDriver("1");
        driver2 = new MarathonDriver("2");
        driver3 = new MarathonDriver("3");
        driver4 = new MarathonDriver("4");
        assertTrue(resultList.add(driver1));
        assertTrue(resultList.add(driver2));
        assertTrue(resultList.add(driver3));

    }

    @AfterEach
    public void tearDown() {
        resultList.clear();
    }

    @Test
    public void testAddDriver() {
        assertTrue(resultList.size() == 3);
        assertFalse(resultList.add(driver1));
        assertFalse(resultList.add(driver1_b));
    }

    @Test
    public void testRemoveDriver() {
        assertTrue(resultList.size() == 3);
        assertTrue(resultList.remove(driver2));
        assertTrue(resultList.size() == 2);
        assertFalse(resultList.remove(driver2));
        assertTrue(resultList.remove(driver1_b));
    }

    @Test
    public void testClear() {
        assertTrue(resultList.size() == 3);
        resultList.clear();
        assertTrue(resultList.size() == 0);
    }

    @Test
    public void testSize() {
        assertTrue(resultList.size() == 3);
        resultList.remove(driver1);
        assertTrue(resultList.size() == 2);
    }

    @Test
    public void testIsEmpty() {
        assertFalse(resultList.isEmpty());
        resultList.clear();
        assertTrue(resultList.isEmpty());
    }

    @Test
    public void testContains() {
        assertTrue(resultList.contains(driver1));
        assertTrue(resultList.contains(driver1_b));
        assertFalse(resultList.contains(driver4));
    }

    @Test
    public void testIterator() {
        //TODO
    }

    @Test
    public void testToArray() {
        Driver[] driverArray = new Driver[3];
        driverArray[0] = driver1;
        driverArray[1] = driver2;
        driverArray[2] = driver3;
        Driver[] resultListArray = resultList.toArray();
        for (int i = 0; i < 3; i++) {
            assertTrue(driverArray[i] == resultListArray[i]);
        }
    }

    @Test
    public void testContainsAll() {
        Collection<Driver> coll = new ArrayList<>();
        coll.add(driver1);
        coll.add(driver2);
        assertTrue(resultList.containsAll(coll));
        coll.add(driver4);
        assertFalse(resultList.containsAll(coll));
    }

    @Test
    public void testRemoveAll() {
        Collection<Driver> coll = new ArrayList<>();
        coll.add(driver1);
        coll.add(driver2);
        assertTrue(resultList.removeAll(coll));
        assertTrue(resultList.size() == 1);
    }

    @Test
    public void testRetainAll() {
        //TODO
    }

    @Test
    public void testReadFile() {
        //TODO
    }

    // @Test
    // public void testWriteFile() throws IOException {
    //     ResultList resultList = new ResultList();
    //     resultList.add(driver1);
    //     resultList.add(driver2);
    //     resultList.add(driver3);
    //     resultList.WriteFile("testfile.txt");
    //     Object[] file1 = Files.lines(Paths.get(String.valueOf(new File("testfile.txt")))).toArray();
    //     Object[] file2 = Files.lines(Paths.get(String.valueOf(new File("testfile copy.txt")))).toArray();
    //     for (int i = 0; i < file1.length; i++) {
    //         assertTrue(file1[i].equals(file2[i]));
    //     }
    // }

    @Test
    public void testEndTimeMissing() {
        resultList.add(driver1);
        driver1.setStart(LocalTime.parse("01:00:00"));
        assertEquals("1; --.--.--; 01.00.00; Slut?", driver1.toFileFormat(false));
    }

    @Test
    public void testStartTimeMissing() {
        resultList.add(driver1);
        driver1.setEnd(LocalTime.parse("01:00:00"));
        assertEquals("1; --.--.--; Start?; 01.00.00", driver1.toFileFormat(false));
    }

    @Test
    public void testSortByStartNumber() {
        resultList.clear();
        resultList.add(driver3);
        resultList.add(driver1);
        resultList.add(driver2);

        resultList.sortByStartNumber();
        assertEquals(driver1, resultList.get(0));
        assertEquals(driver2, resultList.get(1));
        assertEquals(driver3, resultList.get(2));
    }

    @Test
    public void testSort() {
        resultList.clear();
        MarathonDriver d1 = new MarathonDriver("1");
        MarathonDriver d2 = new MarathonDriver("2");
        MarathonDriver d3 = new MarathonDriver("3");
        MarathonDriver d4 = new MarathonDriver("4");
        MarathonDriver d5 = new MarathonDriver("5");

        d1.setStart(LocalTime.now());
        d1.setEnd(d1.getStart().get(0).plusSeconds(60));

        d2.setStart(LocalTime.now());
        //d2.setEnd(d2.getStart().plusSeconds(600));

        d3.setStart(LocalTime.now());
        d3.setEnd(d3.getStart().get(0).plusSeconds(6000));

        d4.setStart(LocalTime.now());
        d4.setEnd(d4.getStart().get(0).plusSeconds(9000));

        d5.setStart(LocalTime.now());
        d5.setEnd(d5.getStart().get(0).plusSeconds(8000));

        resultList.add(d3);
        resultList.add(d1);
        resultList.add(d2);
        resultList.add(d5);
        resultList.add(d4);

        resultList.sort();
        assertEquals(d1, resultList.get(0));
        assertEquals(d3, resultList.get(1));
        assertEquals(d5, resultList.get(2));
        assertEquals(d4, resultList.get(3));
        assertEquals(d2, resultList.get(4));
    }
}
