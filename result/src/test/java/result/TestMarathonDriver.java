package result;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import result.marathon.MarathonDriver;

public class TestMarathonDriver {
    MarathonDriver driver1;

    @BeforeEach
    public void setup() {
        driver1 = new MarathonDriver("1");
    }

    // @Test
    // public void testSetStart() {
    //     driver1.setStart(LocalTime.parse("01:00:00"));
    //     assertEquals(LocalTime.parse("01:00:00"), driver1.getStart());
    // }

    // @Test
    // public void testSetEnd() {
    //     driver1.setEnd(LocalTime.parse("05:00:00"));
    //     assertEquals(LocalTime.parse("05:00:00"), driver1.getEnd());
    // }
    
    @Test
    public void testGetTotalTime() {
        driver1.setStart(LocalTime.parse("01:00:00"));
        driver1.setEnd(LocalTime.parse("05:01:00"));
        assertEquals(LocalTime.parse("04:01:00"), driver1.getTotalTime());
    }
}
